**NOTICE: Do not change the location of playbooks/var/users.yml and keep this repo public. Other applications link directly to this [link](https://gitlab.com/uwmcomputersociety/compsoc-ansible/raw/master/playbooks/vars/userlist.yml) to manage identity.** If it is determined this repo must contain secrets, they could be encrypted with [ansible vault](https://www.digitalocean.com/community/tutorials/how-to-use-vault-to-protect-sensitive-ansible-data-on-ubuntu-16-04).

# OwO What's This?
Ansible is a system configuration tool that can automatically manage servers using ssh. For example, if you have 10 machines and want all of them to have the same users and two of them to have an HTTP server, this can be programatically accomplished with ansible. 

Ansible is a widely-supported configuration management solution that is used by many companies and has many modules to fit different usecases. The API allows for writing your own module or extending what Ansible can do. For more information about this software, you can google around and find some very [developed documentation](http://docs.ansible.com/ansible/latest/user_guide/intro_getting_started.html).

In our case, ansible will be installed on a "master computer" (think of it like a puppetmaster) that issues comamnds to other servers via ssh. Each controlled server's root account trusts a ssh key that the master computer has. Each controlled server needs no extra manual configuration besides the trusting of the master certificate by an account with appropriate access.

## Why do we need something so complicated?
- If all of our servers disappear, we can restore the configuration from this repository if set up correctly.
- We can apply similar configuration (user accounts, apt, webserver configuration, access control, etc.) over an arbitrary amount of servers.
- If you're trying to learn devops, this is the place to start. We're not big enough to use Kubernetes, so check that out on your own time.
- Once there is a developed system configuration framework in place, it's actually easier to configure machines in this way. You only need to edit this repo, not ssh around!

# How to use this repository

## Performing Playbook Functions
1. Clone this to `/etc/ansible` on a computer that has a user that can ssh as root into the configured machines as well as ansible installed. All the machines in the `/etc/ansible/hosts` file need to have an account that has enough access to perform its configured tasks such as configuring users or apt. If you are a user that would not have this access, heck `ansible.cfg` to make sure that the `private_key_file` field points to the correct keyfile.
2. As this user, run `ansible-playbook /etc/ansible/playbooks/[name].yml` to run the specified playbook. These playbooks should be listed below.

Ideally this would be permanently installed on a server that would have this access already, and as a hook would pull the repo and run the playbooks.

## Adding users
1. Fork or branch this repo, and edit `playbooks/var/users.yml` to add both your username and authorized_key. Use the format that the other entries use, with correct whitespace (yaml uses two spaces instead of tabs) and syntax.
2. Make a merge request and some admin will merge this into the master branch if you are authorized.
3. The `users.yml` file can be run as a playbook which will add all the users to the servers in the `memberaccess` group.

Within the system, users are refered to by uid. Therefore it is very important to not change your uid unless absolutely needed, and to make sure no uids conflict with anyone else's. This can be done with a simple Ctl+F or searching for your desired uid. Also make sure there's no repeated pubkeys as this is how each account is authenticated.

# Functions
These are playbook files that actually configure the machines. Edit or add to these to provide functionality! Execute them with `ansible-playbook [yml-file]`

## Users
The `playbooks/users.yml` will configure user accounts. New users will be added here to configure their username, shell, ssh key, and other information. This takes from playbooks/var/userlist.yml.

## Apt
The `playbooks/apt.yml` file configures what additional packages are installed on machines. 

# Configuration

## Ansible master config
The `ansible.cfg` file is the high-level configuration for how ansible will function. See [documentation](http://docs.ansible.com/ansible/latest/reference_appendices/config.html#ansible-configuration-settings). Important lines are the `private_key_file`, and `sudo_user`.

## Host specifications
The `hosts` file delimits which servers ansible will control and their groupings. This file is in TOML format. These groupings can be targeted by playbooks, so all servers of a certain grouping can receive the same configuration.
